//
//  FirebaseSettingsManager.swift
//  Fluffy
//
//  Created by Mac Mini Huenei on 09-02-16.
//  Copyright © 2016 Devin. All rights reserved.
//

import UIKit

class FirebaseSettingsManager: NSObject {
    
    static let sharedInstance = FirebaseSettingsManager()
    
    var firebaseDictionary:NSDictionary
    
    override init() {
        firebaseDictionary = NSDictionary(contentsOfFile:NSBundle.mainBundle().pathForResource("Firebase.Settings", ofType: "plist")!)!
    }
    
    func getFirebaseURL() -> String{
        return firebaseDictionary[kFirebaseURL] as! String
    }
}
