//
//  LoginViewController.swift
//  Fluffy
//
//  Created by Mac Mini Huenei on 08-02-16.
//  Copyright © 2016 Devin. All rights reserved.
//

import UIKit
import SpinKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    @IBOutlet weak var logoConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernameConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomRegisterConstraint: NSLayoutConstraint!
    @IBOutlet weak var fbButtonContainer: UIView!
    
    let kLogoDistance = 25.0
    let kUsernameDistance = 30.0
    let kBottomRegisterDistance = 30.0
    var isKeyboardShown = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for v:UIView in self.view.subviews {
            if v.isFirstResponder() {
                v.resignFirstResponder()
            }
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    func keyboardWillShow(){
        if !isKeyboardShown {
            UIView.animateWithDuration(0.2) { () -> Void in
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.logoConstraint.constant -= CGFloat(self.kLogoDistance)
                    self.usernameConstraint.constant -= CGFloat(self.kUsernameDistance)
                    self.bottomRegisterConstraint.constant -= CGFloat(self.kBottomRegisterDistance)
                    
                })
            }
            isKeyboardShown = true
        }
    }
    
    func keyboardWillHide(){
        if isKeyboardShown {
            UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
                self.logoConstraint.constant += CGFloat(self.kLogoDistance)
                self.usernameConstraint.constant += CGFloat(self.kUsernameDistance)
                self.bottomRegisterConstraint.constant += CGFloat(self.kBottomRegisterDistance)
                }, completion: nil)
            isKeyboardShown = false
        }
    }
    
    @IBAction func ingresarTouched(sender: AnyObject) {
        LoginManager.sharedInstance.authenticateUser(usernameTxt.text!, password: passwordTxt.text!, completion: {(response) -> Void in
            
        })
    }
    
    @IBAction func facebookLoginTouched(sender: AnyObject) {
        LoginManager.sharedInstance.authenticateUserFromFacebook(self)
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    
}

extension UITextField {
    
    func useUnderline(color: UIColor) {
        
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = color.CGColor
        border.frame = CGRectMake(0, self.frame.size.height - borderWidth, self.frame.size.width, self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
