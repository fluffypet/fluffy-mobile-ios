//
//  LoginManager.swift
//  Fluffy
//
//  Created by Mac Mini Huenei on 09-02-16.
//  Copyright © 2016 Devin. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit

class LoginManager: NSObject {
    
    static let sharedInstance = LoginManager()
    
    let ref = Firebase(url: FirebaseSettingsManager.sharedInstance.getFirebaseURL())
    
    func authenticateUser(username: String, password: String, completion: NSDictionary? -> Void){
        ref.authUser(username, password: password) { (error, data) -> Void in
            if let err = error {
                print(err.description)
                completion(nil)
            } else {
                print(data.uid)
                let refUsers = self.ref.childByAppendingPath("users/\(data.uid)")
                refUsers.observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
                    print(snapshot.value)
                    self.saveProfileToStandardDefaults(snapshot.value)
                    completion(snapshot.value as? NSDictionary)
                })
            }
        }
    }
    
    func createAuthWithData(userData: NSDictionary){
        ref.createUser(userData["email"] as! String, password: userData["password"] as! String) { (error, value) -> Void in
            
        }
    }
    
    func createUserWithRegisterData(uid: String, registerData: NSDictionary) {
        let refUsers = ref.childByAppendingPath("users")
        refUsers.setValue([uid : registerData])
    }
    
    func updateUserWithRegisterData(uid: String, registerData: NSDictionary) {
        let refUsers = ref.childByAppendingPath("users")
        refUsers.updateChildValues([uid: registerData])
    }
    
    func authenticateUserFromFacebook(context: UIViewController){
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logInWithReadPermissions(["public_profile","email"], fromViewController: context) { (result, error) -> Void in
            self.ref.authWithOAuthProvider("facebook", token: result.token.tokenString) { (error, oAuthData) -> Void in
                let refUser = self.ref.childByAppendingPath("users/\(oAuthData)")
                refUser.observeSingleEventOfType(.Value, withBlock: { (snapshot) -> Void in
                    let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "email, name, gender, picture"])
                    graphRequest.startWithCompletionHandler({ (connection, fbresponse, error) -> Void in
                        if snapshot.value is NSNull {
                            self.createUserWithRegisterData(oAuthData.uid, registerData: ["email": fbresponse["email"] as! String, "name": fbresponse["name"] as! String, "age": 0, "gender": fbresponse["gender"] as! String])
                        } else {
                            self.updateUserWithRegisterData(oAuthData.uid, registerData: ["email": fbresponse["email"] as! String, "name": fbresponse["name"] as! String, "age": 0, "gender": fbresponse["gender"] as! String])
                        }
                    })
                })
            }
        }
        
        
    }
    
    func saveProfileToStandardDefaults(profile:AnyObject){
        NSUserDefaults.standardUserDefaults().setObject(profile as! NSDictionary, forKey: "profileInfo")
    }
}
